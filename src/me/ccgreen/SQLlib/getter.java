package me.ccgreen.SQLlib;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import com.sun.rowset.CachedRowSetImpl;
import java.util.concurrent.Callable;

class getter implements Callable<ResultSet> {

	Connection connection;
	String condition;
	String table;
	
	getter(Connection connection, String table, String condition){
		this.connection = connection;
		this.table = table;
		this.condition = condition;
	}

	@Override
	public ResultSet call() throws Exception {
		Statement stmt;
		String sql = "SELECT * FROM " + table + " WHERE " + condition + ";";

		stmt = connection.createStatement();
		ResultSet result = stmt.executeQuery(sql);

		CachedRowSetImpl crs = new CachedRowSetImpl();
        crs.populate(result);

        result.close();

		return crs;
	}
	
}