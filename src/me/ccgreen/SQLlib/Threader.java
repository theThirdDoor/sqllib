package me.ccgreen.SQLlib;

import java.sql.*;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.bukkit.Bukkit;

// Referenced classes of package me.ccgreen.SQLlib:
//            SQLlibMain, setter, deleter, initialiser, 
//            statementSender, getter

class Threader
{

	private String host;
	private String username;
	private String password;
	private String database;
	private int port;
	private static Connection connection;
	private ExecutorService threads;

	Threader(SQLlibMain main, int por, String hos, String data, String user, String pass, int threadCount) throws SQLException {
		port = por;
		host = hos;
		database = data;
		username = user;
		password = pass;
		try {
			openConnection();
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
			SQLlibMain.printInfo("Class not found exception!");
		}
		threads = Executors.newFixedThreadPool(threadCount);
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(main, new Runnable(){ 

			@Override 
			public void run() { 
				try { 
					Threader.connection.close(); 
					
					if(Threader.connection != null && !Threader.connection.isClosed()) {
						return;
					}
					Class.forName("com.mysql.jdbc.Driver");
					Threader.connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password); 

					
				} catch (SQLException | ClassNotFoundException e) { 
					// TODO Auto-generated catch block 
					e.printStackTrace(); 
				}
			} 

		}, 6000, 6000); 

	}

	void reload() { 
		try { 
			connection.close(); 
			openConnection(); 
		} catch (SQLException | ClassNotFoundException e) { 
			// TODO Auto-generated catch block 
			e.printStackTrace(); 
		}
	}

	void set(String table, String feilds, String values) {
		threads.execute(new setter(connection, table, feilds, values));
	}

	void delete(String table, String condition) {
		threads.execute(new deleter(connection, table, condition));
	}

	void initialiseTable(String table, String pKey, String args) {
		threads.execute(new initialiser(connection, table, pKey, args));
	}

	void sendBatch(Vector<String> statement) {
		threads.execute(new statementSender(connection, statement));
	}

	void sendBatchOnMainThread(Vector<String> statement) {
		statementSender sender = new statementSender(connection, statement);
		sender.run();
	}

	ResultSet get(String table, String Condition) {
		getter get = new getter(connection, table, Condition);
		try {
			return get.call();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	void disable() {
		try {
			connection.close();
			threads.shutdown();
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}

	void openConnection() throws SQLException, ClassNotFoundException {
		if(connection != null && !connection.isClosed()) {
			return;
		}
		Class.forName("com.mysql.jdbc.Driver");
		connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password); 
	}
}