package me.ccgreen.SQLlib;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

class statementSender implements Runnable {

	Connection connection;
	Vector<String> passedStrings;
	
	statementSender(Connection connection, Vector<String> statement){
		this.connection = connection;
		this.passedStrings = statement;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		Statement stmt;
		try {
			stmt = connection.createStatement();
			for(String s: passedStrings) {
				stmt.addBatch(s);
			}
			stmt.executeLargeBatch();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			SQLlibMain.printInfo("batch upload failed");
		}
		
	}
	
}