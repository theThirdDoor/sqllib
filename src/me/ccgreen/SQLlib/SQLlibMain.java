package me.ccgreen.SQLlib;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class SQLlibMain extends JavaPlugin {
	
	private static ConsoleCommandSender CONSOLE;
	public static boolean configSetup = false;
	static Threader threader;
	static String prefix = "SQLlib_";
	static String uuidTable = prefix + "nameToUuid";
	
	public void onEnable(){
		File configFile = new File(getDataFolder(), "config.yml");	
		CONSOLE = Bukkit.getServer().getConsoleSender();
		
		if(!configFile.exists()){
			new File(this.getDataFolder() + "").mkdir();

			try {
				if(configFile.createNewFile()){
					try {
						PrintWriter writer = new PrintWriter(configFile);

						writer.write("#Developer : ccgreen\n"
								+ "Host: \n"
								+ "Port: \n"
								+ "Database: \n"
								+ "Username: \n"
								+ "Password: \n"
								+ "ThreadCount: 2"
								);
						writer.close();
						printInfo("Config file created, please fill in before next launch");
					} catch (IOException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
		} else {
			YamlConfiguration configYml = YamlConfiguration.loadConfiguration(configFile);
			String host, database, username, password;
			int port, threads;
			host = configYml.getString("Host");
			port = configYml.getInt("Port");
			database = configYml.getString("Database");
			username = configYml.getString("Username");
			password = configYml.getString("Password");
			threads = configYml.getInt("ThreadCount");
			configSetup = true;
			
			try {
				threader = new Threader(this, port, host, database, username, password, threads);
			} catch(SQLException e) {
				configSetup = false;
				e.printStackTrace();
				SQLlibMain.printInfo("Please check the config is filled out correctly!");
			}
			
			if(configSetup) {
				getCommand("testSQL").setExecutor(new CommandListener(this));
			} else {
				getServer().getPluginManager().disablePlugin(this);
			}
			initialiseTable(uuidTable, "username", "`username` VARCHAR(16) NOT NULL , `uuid` VARCHAR(36) NOT NULL");
			getServer().getPluginManager().registerEvents(new EntryListener(this), this);
		}
	}
	
	public void onDisable() {
		if(configSetup) {
			threader.disable();
		}
	}
	
	public void delete(String table, String condition) {
		threader.delete(table, condition);
	}
	
	public void set(String table, String feilds, String values) {
		threader.set(table, feilds, values);
	}
	
	public void initialiseTable(String table, String pKey, String args) {
		threader.initialiseTable(table, pKey, args);
	}
	
	public void sendBatch(Vector<String> batchStatement) {
		threader.sendBatch(batchStatement);
	}
	
	public void sendBatchOnMainThread(Vector<String> batchStatement) {
		threader.sendBatchOnMainThread(batchStatement);
	}
	
	public ResultSet get(String table) {
		return threader.get(table, "1=1");
	}
	
	public ResultSet get(String table, String Condition) {
		return threader.get(table, Condition);
	}
	
	public boolean safetyCheck() {
		return configSetup;
	}
	
	static void printInfo(String line){
		CONSOLE.sendMessage(ChatColor.GREEN + "[SQL] : " + line);
	}
	
}