package me.ccgreen.SQLlib;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

class deleter implements Runnable {

	Connection connection;
	String table;
	String condition;
	
	deleter(Connection connection, String table, String condition){
		this.connection = connection;
		this.table = table;
		this.condition = condition;
	}
	
	@Override
	public void run() {
		Statement stmt;
		String sql ="DELETE FROM " + table + " WHERE " + condition + ";";
		
		try {
			stmt = connection.createStatement();
			stmt.execute(sql);
			
		} catch (SQLException e) {
			SQLlibMain.printInfo("SQL exception, check your sql: " + sql);
			e.printStackTrace();
		}
		
	}
	
}