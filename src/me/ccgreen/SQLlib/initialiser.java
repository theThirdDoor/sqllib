package me.ccgreen.SQLlib;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

class initialiser implements Runnable{

	Connection connection;
	String table;
	String pKey;
	String args;

	initialiser(Connection connection, String table, String pKey, String args){
		this.connection = connection;
		this.table = table;
		this.pKey = pKey;
		this.args = args;
	}

	@Override
	public void run() {
		Statement stmt = null;
		
		String sql = "CREATE TABLE IF NOT EXISTS " + table + " ( " + args + ", PRIMARY KEY (" + pKey + ")) ENGINE = InnoDB;";
		
		try {
			stmt = connection.createStatement();
			stmt.execute(sql);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			SQLlibMain.printInfo("SQL entered was: '" + sql + "'");
		}	
	}
}