package me.ccgreen.SQLlib;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;


public class EntryListener implements Listener {

	  private final SQLlibMain plugin;

	  public EntryListener(SQLlibMain plugin) {
	    this.plugin = plugin;
	  }

	  @EventHandler
	  public void OnPlayerConnect(PlayerJoinEvent event){
	    Player p = event.getPlayer();
	    String uuid = p.getUniqueId().toString();
	    String username = p.getDisplayName().toString();
	    plugin.set(SQLlibMain.uuidTable, "uuid, username", "'"+uuid + "', '" + username + "'");
	  }
};