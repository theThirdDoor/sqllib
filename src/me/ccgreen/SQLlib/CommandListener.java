package me.ccgreen.SQLlib;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

public class CommandListener implements CommandExecutor {


	private final SQLlibMain Main;


	public CommandListener(SQLlibMain plugin) {
		Main = plugin;
	}



	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		if (sender instanceof ConsoleCommandSender) {
			if (commandLabel.equalsIgnoreCase("testsql")) {
				if(args.length == 0){
					printCommands();
				}
				if(args.length > 0) {
					if(args[0].equalsIgnoreCase("reload")) {
						SQLlibMain.threader.reload();
					}
					if(args[0].equalsIgnoreCase("set")) {
						if(args.length == 1) {
							SQLlibMain.printInfo("testsql set <string> <int>");
						}else
							if(args.length == 2) {
								Main.set("SQLlib_Test", "player, points", "'" + args[1] + "', '0'");
							} else	{
								Main.set("SQLlib_Test", "player, points", "'" + args[1] + "', '" + args[2] + "'");							
							}
					} else if(args[0].equalsIgnoreCase("get")) {
						ResultSet results;
						results = Main.get("SQLlib_Test");
						try {
							while(results.next()) {
								String player = results.getString("player");
								int points = results.getInt("points");
								SQLlibMain.printInfo("Player " + player + " has " + points + " points.");
							}
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return false;
						}
					} else if (args[0].equalsIgnoreCase("init")){
						SQLlibMain.printInfo("Creating test table");
						Main.initialiseTable("SQLlib_Test", "player", "player VARCHAR(80), points INT");
						SQLlibMain.printInfo("Test table initialised!");
					} else {
						printCommands();
					}
				}
			}
		}
		return false;
	}
	void printCommands() {
		SQLlibMain.printInfo("testsql set <string> <int>   |   testsql get    |    testsql init");
	}
}